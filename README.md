# basic-html-css

basic html dan css mantaph

# TUGAS Day 1 

1. Create summary about today lesson

# TUGAS Day 2

1. Create summary about today lesson
2. Github : Make repo dan push :
a.) Tugas day 1
b.) Tugas day 2
3. Teori : 
- apa itu git ?
- untuk apa fungsi dari command :
    + git init
    + git remote add origin <link_name>
    + git add <file_name>
    + git commit -m "<message_name>"
    + git push origin master

# TUGAS Day 3

1. Create summary about today lesson
2. Teori :
- untuk apa fungsi dari command :
    + git checkout -b <branch_name>
    + git pull origin <branch_name>
   
3. Aplikasikan git branch dan merge ke dalam sebuah website
Ketentuan : (Repo public)
+ Buat 2/3 page html : index.html, ..., ...
+ Tentukan siapa PM nya, Repo -> clone ke si A, si B
+ PM -> megang kendali branch master
contoh : A = PM , B = developer, C = developer
A -> branch master, branch A-dev
B -> branch B-dev
C -> branch C-dev

# TEAM
1. Dini + Apson             = 2 page
2. Bastian + Abdul          = 2 page
3. Julia + Daniel + Najib   = 3 page